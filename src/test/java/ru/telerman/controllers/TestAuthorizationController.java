package ru.telerman.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.ModelAndView;
import ru.telerman.configuration.MvcConfig;
import ru.telerman.configuration.security.SecurityConfiguration;
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {MvcConfig.class, SecurityConfiguration.class})
public class TestAuthorizationController {

    private MockMvc mockMvc;
    @Autowired
    private AuthorisationController controller;


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testRegistrationFormMethod() {
        ModelAndView modelAndView = controller.showRegistrationForm();
        Assert.assertNotNull(modelAndView);
        Assert.assertNotNull(modelAndView.getModel());
        Assert.assertTrue(modelAndView.getModel().size() == 1);
    }

    @Test
    public void testRegistrationUser(){
        /*ModelAndView modelAndView = controller.registerUser(null);
        Assert.assertNotNull(modelAndView);
        Assert.assertNotNull(modelAndView.getView());

        UserDto userDto = new UserDto();
        userDto.setLogin("test");
        userDto.setPassword("1234");
        userDto.setMatchingPassword("1234");
        ModelAndView modelAndView1 = controller.registerUser(userDto);*/

    }
}
