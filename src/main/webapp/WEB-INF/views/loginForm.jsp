<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<h1>Login</h1>
<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
<c:out value="${errorString}"/>
<form name='f' action="login" method='POST'>
    <table>
        <tr>
            <td>User:</td>
            <td><input name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password' /></td>
        </tr>
        <tr>
            <td>Remember Me:</td>
            <td><input type="checkbox" name="remember-me" /></td>
        </tr>
        <tr>
            <td><input name="submit" type="submit" value="Login" /></td>
            <td><input type="button" onclick="location.href='/registration'" value="Register new" /></td>
        </tr>
        <tr>
            <td><a href="/oauth2/authorization/google">Google</a></td>
            <td><a href="/oauth2/authorization/facebook">Facebook</a></td>
        </tr>
    </table>
</form>
</body>
</html>