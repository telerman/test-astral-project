<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Note '${note.header}' editor</title>
</head>
<body>
<form:form method="post" modelAttribute="note" action="/updateNote">
    <form:hidden path="id"/>
    <form:hidden path="icon"/>
    <table border="0" width="100%">
        <tr>
            <td>Icon:</td>
            <td>
                <svg width="90" height="90">
                    <image xlink: xlink:href="data:image/svg+xml;base64,${note.icon}" width="90" height="90"/>
                </svg>
            </td>
        </tr>
        <tr>
            <td>Header:</td>
            <td width="90%"><form:input path="header"/> <br/></td>
        </tr>
        <tr>
            <td>Body:</td>
            <td width="90%"><form:textarea path="body" rows="15" cols="60"/> <br/></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <form:button>Save</form:button>
                <input type="button" onclick="location.href='/'" value="Cancel">
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>