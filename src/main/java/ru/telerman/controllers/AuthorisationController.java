package ru.telerman.controllers;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.apis.GoogleApi20;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ru.telerman.model.UserDto;
import ru.telerman.security.UserDao;
import ru.telerman.services.OauthService;

import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
public class AuthorisationController {
    @Qualifier("userDaoImpl")
    @Autowired
    private UserDao userDao;
    @Autowired
    private OauthService oauthService;
    @Qualifier("customAuthProvider")
    @Autowired
    private AuthenticationProvider authProvider;


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView showRegistrationForm() {
        return new ModelAndView("registrationForm", "user", new UserDto());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registerUser(@ModelAttribute("user") UserDto user) throws UnsupportedEncodingException {
        String s = userDao.registerUser(user);
        if (s == null)
            return new ModelAndView("redirect:/login");
        else return new ModelAndView("redirect:/login?error=" + URLEncoder.encode(s, "UTF-8"));
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(
            @RequestParam(value = "error", required = false) String error,
            ModelMap model
    ) {
        if (error != null && !error.isEmpty())
            model.addAttribute("errorString", error);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken)
            return "loginForm";
        else return "redirect:/";
    }

    @GetMapping("/oauth2/authorization/google")
    public String googleLogin(ModelMap modelMap, HttpSession session) throws UnsupportedEncodingException {
        try {
            return oauthService.getRedirectUrl(session.getId(), GoogleApi20.instance());
        } catch (Exception e) {
            return "redirect:/login?error=" + getErrorMessage(e);
        }
    }

    @GetMapping("/callback/google")
    public ModelAndView googleCallback(
            @RequestParam("code") String code,
            @RequestParam("state") String state,
            HttpSession session
    ) throws UnsupportedEncodingException {
        try {
            Authentication authentication = oauthService.processGoogleCallback(session.getId(), code, state);
            authProvider.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new ModelAndView("redirect:/");
        } catch (Exception e) {
            return new ModelAndView("redirect:/login?error=" + getErrorMessage(e));
        }
    }

    @GetMapping("/oauth2/authorization/facebook")
    public String facebookLogin(ModelMap modelMap, HttpSession session) throws UnsupportedEncodingException {
        try {
            return oauthService.getRedirectUrl(session.getId(), FacebookApi.instance());
        } catch (Exception e) {
            return "redirect:/login?error=" + getErrorMessage(e);
        }
    }

    @GetMapping("/callback/facebook")
    public ModelAndView facebookCallback(
            @RequestParam("code") String code,
            @RequestParam("state") String state,
            HttpSession session
    ) {
        try {
            Authentication authentication = oauthService.processFacebookCallback(session.getId(), code, state);
            authProvider.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new ModelAndView("redirect:/");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getErrorMessage(Exception e) throws UnsupportedEncodingException {
        String error;
        if(e.getLocalizedMessage() != null)
            error = e.getLocalizedMessage();
        else error = e.getClass().toString();
        return URLEncoder.encode(error, "UTF-8");
    }
}
