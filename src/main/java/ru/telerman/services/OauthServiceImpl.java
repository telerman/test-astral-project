package ru.telerman.services;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.BaseApi;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import ru.telerman.model.OauthData;
import ru.telerman.security.CustomAuthentication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

@Service
@PropertySource("classpath:application.properties")
public class OauthServiceImpl implements OauthService {

    @Autowired
    private Environment environment;
    private Map<String, OauthData> requestsData = new HashMap<>();

    @Override
    public Authentication processGoogleCallback(String sessionId, String code, String state)
            throws InterruptedException, ExecutionException, IOException, ParseException {
        //Обработка входных данных
        OauthData oauthData = requestsData.get(sessionId);
        if (oauthData != null)
            requestsData.remove(sessionId);
        else return null;
        String secretState = oauthData.getSecretState();
        if (!secretState.equals(state))
            throw new InterruptedException("Секретные ключи не совпадают");
        OAuth20Service service = oauthData.getService();
        OAuth2AccessToken accessToken = service.getAccessToken(code);
        final String requestUrl = "https://www.googleapis.com/oauth2/v2/userinfo";
        final OAuthRequest request = new OAuthRequest(Verb.GET, requestUrl);
        service.signRequest(accessToken, request);
        final Response response = service.execute(request);

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
            builder.append(line);
        JSONObject parse = (JSONObject) new JSONParser().parse(builder.toString());
        Object id = parse.get("id");
        if (id == null)
            throw new InterruptedException("Идентификатор пользователя не найден");

        //Создание объекта авторизации
        return new CustomAuthentication((String) id);
    }

    @Override
    public String getRedirectUrl(String sessionId, BaseApi<OAuth20Service> apiType) throws AuthenticationException {
        String paramName;
        if (apiType instanceof GoogleApi20)
            paramName = "google";
        else if (apiType instanceof FacebookApi)
            paramName = "facebook";
        else return "redirect:/";

        final String clientId = environment.getProperty(String.format("spring.security.oauth2.client.registration.%s.client-id", paramName));
        final String clientSecret = environment.getProperty(String.format("spring.security.oauth2.client.registration.%s.client-secret", paramName));
        final String secretState = "secret" + new Random().nextInt(999_999);

        if(clientId == null || clientSecret == null){
            throw new AuthenticationServiceException("Не заданы параметры подключения внешнего провайдера");
        }
        ServiceBuilder serviceBuilder = new ServiceBuilder(clientId)
                .apiSecret(clientSecret)
                .callback("http://localhost:8080/callback/" + paramName);
        if (paramName.equals("google"))
            serviceBuilder = serviceBuilder.scope("profile");
        final OAuth20Service service = serviceBuilder
                .build(apiType);
        final Map<String, String> additionalParams = new HashMap<>();
        additionalParams.put("access_type", "offline");
        additionalParams.put("prompt", "consent");
        final String authorizationUrl = service.getAuthorizationUrl(secretState, additionalParams);
        OauthData oauthData = new OauthData(sessionId, secretState, service);
        requestsData.put(oauthData.getSessionId(), oauthData);
        return "redirect:" + authorizationUrl;
    }

    @Override
    public Authentication processFacebookCallback(String sessionId, String code, String state)
            throws InterruptedException, ExecutionException, IOException, ParseException {
        OauthData oauthData = requestsData.get(sessionId);
        if (oauthData != null)
            requestsData.remove(sessionId);
        else return null;
        String secretState = oauthData.getSecretState();
        if (!secretState.equals(state))
            throw new InterruptedException("Секретные ключи не совпадают");
        OAuth20Service service = oauthData.getService();
        OAuth2AccessToken accessToken = service.getAccessToken(code);
        final OAuthRequest request = new OAuthRequest(Verb.GET, "https://graph.facebook.com/v2.11/me");
        service.signRequest(accessToken, request);
        final Response response = service.execute(request);

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
            builder.append(line);
        JSONObject parse = (JSONObject) new JSONParser().parse(builder.toString());
        Object id = parse.get("id");
        if (id == null)
            throw new InterruptedException("Идентификатор пользователя не найден");

        //Создание объекта авторизации
        return new CustomAuthentication((String) id);
    }

}
