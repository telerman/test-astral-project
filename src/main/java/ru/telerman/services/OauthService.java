package ru.telerman.services;

import com.github.scribejava.core.builder.api.BaseApi;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.json.simple.parser.ParseException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface OauthService {

    Authentication processGoogleCallback(String sessionId, String code, String state) throws InterruptedException, ExecutionException, IOException, ParseException, AuthenticationException;

    Authentication processFacebookCallback(String sessionId, String code, String state) throws InterruptedException, ExecutionException, IOException, ParseException, AuthenticationException;

    String getRedirectUrl(String sessionId, BaseApi<OAuth20Service> apiType) throws AuthenticationException;
}
