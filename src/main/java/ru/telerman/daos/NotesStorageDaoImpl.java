package ru.telerman.daos;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import ru.telerman.model.Note;
import ru.telerman.utils.mappers.NoteMapper;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Repository
public class NotesStorageDaoImpl implements NotesStorageDao {
    @Inject
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Note> getList() throws DataAccessException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return null;
        final String sql = "SELECT * FROM notes n WHERE n.create_by = ? ORDER BY n.create_ts DESC";
        List<Note> list = jdbcTemplate.query(sql, new NoteMapper(), authentication.getName());
        return list;
    }

    @Override
    public List<Note> getList(String searchString) throws DataAccessException {
        if (searchString == null || searchString.isEmpty())
            return getList();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return null;
        final String sql = "SELECT * FROM notes n WHERE (upper(n.header) LIKE ? OR upper(n.body) LIKE ?) AND create_by = ? ORDER BY create_ts DESC";
        String stParam = '%' + searchString.toUpperCase() + '%';
        List<Note> notes = jdbcTemplate.query(sql, new NoteMapper(), stParam, stParam, authentication.getName());
        return notes;
    }

    @Override
    public Note get(long id) throws DataAccessException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return null;
        final String sql = "SELECT * FROM NOTES n WHERE n.ID = ? AND n.create_by = ?";
        List<Note> query = jdbcTemplate.query(sql, new NoteMapper(), id, authentication.getName());
        return query.size() == 0 ? null : query.get(0);
    }

    @Override
    public void update(Note note) throws DataAccessException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return;
        final String sql = "UPDATE notes n SET n.header = ?, n.body = ? WHERE n.id = ? AND n.create_by = ?";
        int update = jdbcTemplate.update(sql, note.getHeader(), note.getBody(), note.getId(), authentication.getName());
        if (update == 0)
            add(note);
    }

    @Override
    public void delete(long id) throws DataAccessException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return;
        final String sql = "DELETE FROM notes WHERE id = ? AND create_by = ?";
        jdbcTemplate.update(sql, id, authentication.getName());
    }

    private void add(Note note) throws DataAccessException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null)
            return;
        final String sql = "INSERT INTO notes(header, body, icon, create_ts, create_by) VALUES (?,?,?,?,?)";
        jdbcTemplate.update(sql, note.getHeader(), note.getBody(), note.getIcon(),
                new Timestamp(new Date().getTime()),
                authentication.getName());
    }

}
