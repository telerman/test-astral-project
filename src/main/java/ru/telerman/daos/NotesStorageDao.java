package ru.telerman.daos;


import org.springframework.dao.DataAccessException;
import ru.telerman.model.Note;

import java.util.List;


public interface NotesStorageDao {

    List<Note> getList() throws DataAccessException;

    List<Note> getList(String searchString) throws DataAccessException;

    Note get(long id) throws DataAccessException;

    void update(Note note) throws DataAccessException;

    void delete(long id) throws DataAccessException;

}
