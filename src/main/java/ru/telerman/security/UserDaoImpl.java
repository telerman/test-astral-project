package ru.telerman.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import ru.telerman.model.UserDto;
import ru.telerman.utils.mappers.UserDtoMapper;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User findUser(String login) throws DataAccessException {
        final String sql = "SELECT * FROM users WHERE username = ?";
        
        List<UserDto> users = jdbcTemplate.query(sql, new UserDtoMapper(), login);
        if (users.size() > 1)
            throw new UsernameNotFoundException("Not one user found for current login");
        else if (users.size() == 0)
            return null;
        else {
            UserDto userDto = users.get(0);
            return new User(userDto.getLogin(), userDto.getPassword(), new ArrayList<>());
        }
    }

    @Override
    public String registerUser(UserDto user) {
        if (user.getLogin() == null || user.getPassword() == null && user.getMatchingPassword() == null ||
                user.getLogin().isEmpty() || user.getPassword().isEmpty() || user.getMatchingPassword().isEmpty())
            return "Заполните все поля!";
        if (!user.getPassword().equals(user.getMatchingPassword()))
            return "Пароли не совпадают";

        try {
            String sql = "SELECT count(*) FROM users WHERE username = ?";
            Integer count = jdbcTemplate.query(sql, (resultSet, i) -> resultSet.getInt(1), user.getLogin()).get(0);
            if (count > 0)
                return "Пользователь уже существует";
            else {
                sql = "INSERT INTO users(username, password) VALUES (?,?)";
                jdbcTemplate.update(sql, user.getLogin(), passwordEncoder.encode(user.getPassword()));
                return null;
            }
        } catch (DataAccessException e) {
            return e.getMessage();
        }
    }
}
