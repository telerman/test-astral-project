package ru.telerman.security;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.User;
import ru.telerman.model.UserDto;

public interface UserDao {
    User findUser(String login) throws DataAccessException;

    String registerUser(UserDto user);
}
