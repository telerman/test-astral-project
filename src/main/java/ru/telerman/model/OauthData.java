package ru.telerman.model;

import com.github.scribejava.core.oauth.OAuth20Service;

public class OauthData {
    private String sessionId;
    private String secretState;
    private OAuth20Service service;

    public OauthData(String sessionId, String secretState, OAuth20Service service) {
        this.sessionId = sessionId;
        this.secretState = secretState;
        this.service = service;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSecretState() {
        return secretState;
    }

    public void setSecretState(String secretState) {
        this.secretState = secretState;
    }

    public OAuth20Service getService() {
        return service;
    }

    public void setService(OAuth20Service service) {
        this.service = service;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof OauthData))
            return false;
        OauthData obj2 = (OauthData) obj;
        return this.sessionId.equals(obj2.sessionId);
    }

    @Override
    public int hashCode() {
        return this.sessionId.hashCode();
    }
}
