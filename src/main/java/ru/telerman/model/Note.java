package ru.telerman.model;

import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class Note {
    private Long id;
    private String header;
    private String body;
    private String icon;
    private Date createTs;
    private String createBy;

    public Note() {
    }

    public Note(Long id) {
        this.id = id;
    }

    public Note(Long id, String header, String body, String icon, String createBy, Date createTs) {
        this.id = id;
        this.header = header;
        this.body = body;
        this.icon = icon;
        this.createBy = createBy;
        this.createTs = createTs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
}
