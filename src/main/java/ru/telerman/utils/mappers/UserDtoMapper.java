package ru.telerman.utils.mappers;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import ru.telerman.model.UserDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDtoMapper implements RowMapper<UserDto> {

    @Nullable
    @Override
    public UserDto mapRow(ResultSet resultSet, int i) throws SQLException {
        UserDto userDto = new UserDto();
        userDto.setLogin(resultSet.getString("username"));
        userDto.setPassword(resultSet.getString("password"));
        userDto.setMatchingPassword(resultSet.getString("password"));
        return userDto;
    }
}
