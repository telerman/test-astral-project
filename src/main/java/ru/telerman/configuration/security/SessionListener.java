package ru.telerman.configuration.security;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setMaxInactiveInterval(15*60);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {}
}
